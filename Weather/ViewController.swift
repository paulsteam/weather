//
//  ViewController.swift
//  Weather
//
//  Created by Paul Pearson on 2/13/16.
//  Copyright © 2016 RPM Consulting. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextViewDelegate {

    @IBOutlet var cityField: UITextField!
    @IBOutlet var weatherText: UITextView!
    
    let url = NSURL(string:"http://app.boxstormstaging.com")!

    @IBAction func getWeather(sender: AnyObject) {
        if ((cityField.text?.isEmpty) == true) {
            print("not doing anything, empty text field")
            return
        }
        let trimmedString = cityField.text!.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet()
        )
        let city = NSString(string: trimmedString)
        let parts = city.componentsSeparatedByString(" ")
        let newCity = NSString(string: parts.joinWithSeparator("-"))
        
        let attemptedUrl = NSURL(string:"http://www.weather-forecast.com/locations/"+(newCity as String)+"/forecasts/latest")
        if let url = attemptedUrl {
        let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) -> Void in
            // Closure/Completion Handler, will happen when task completes
            if let urlContent = data {
                let webContent = NSString(data: urlContent, encoding: NSUTF8StringEncoding)
                let forecastText = self.getCityForecast(webContent!, city: self.cityField.text!)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.weatherText.text = forecastText
                })
            } else {
                // show error message
            }
            
        }
        task.resume()
        }
        
        
    }
    
    func getCityForecast(var content: NSString, city: String) -> String {
        var forecastText = "No forecast for "+city
        let startText = "1 &ndash; 3 Day Weather Forecast Summary:</b><span class=\"read-more-small\"><span class=\"read-more-content\"> <span class=\"phrase\">"
        let endText = "</span></span></span></p>"
        
        if content.containsString(startText) {
            print("found the start text!")
            var range = content.rangeOfString(startText)
            content = content.substringFromIndex(range.location+range.length)
            range = content.rangeOfString(endText)
            content = content.substringToIndex(range.location)
            content = content.stringByReplacingOccurrencesOfString("&deg;", withString: "º")
            forecastText = "Forecast for "+city+"\n\n"+String(content)
        }
        return forecastText
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

